export function formatDateTime(timestamp) {
  const dateObj = new Date(timestamp);
  const date = dateObj.toDateString().split(' ').slice(1).join(' ');
  const time = dateObj.toLocaleTimeString([], {
    hour: '2-digit',
    minute: '2-digit',
  });
  const toISODateOnly = dateObj.toISOString().split('T')[0];

  return { date, time, toISODateOnly };
}

export default { formatDateTime };
