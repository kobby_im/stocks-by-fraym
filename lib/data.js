import { formatDateTime } from '../utils';
import config from './config';

const { API_URL } = config;
const { API_KEY } = config;

function format(res) {
  const columnNames = res.column_names;
  const timeColumnPosition = columnNames.indexOf('Date');
  const openColumnPosition = columnNames.indexOf('Open');
  const closeColumnPosition = columnNames.indexOf('Close');
  const lowColumnPosition = columnNames.indexOf('Low');
  const highColumnPosition = columnNames.indexOf('High');

  const data = res.data.map((datum) => ({
    x: new Date(datum[timeColumnPosition]).getTime(),
    open: datum[openColumnPosition],
    close: datum[closeColumnPosition],
    low: datum[lowColumnPosition],
    high: datum[highColumnPosition],
  }));
  return {
    data,
    frequency: res.frequency,
    startDate: res.start_date,
    endDate: res.end_date,
    minDate: res.oldest_available_date,
    maxDate: res.newest_available_date,
  };
}

async function getHistoricalData(datasetCode, startDate, endDate) {
  let startDateQuery; let endDateQuery = '';
  if (startDate) {
    startDateQuery = `&start_date=${formatDateTime(startDate).toISODateOnly}`;
  }
  if (startDate) {
    endDateQuery = `&end_date=${formatDateTime(endDate).toISODateOnly}`;
  }
  const url = `${API_URL}/WIKI/${datasetCode}/data.json?order=asc${startDateQuery}${endDateQuery}&api_key=${API_KEY}`;
  return fetch(url).then((res) => {
    if (!res.ok) {
      throw Error(res.statusText);
    }
    return res.json();
  }).then((res) => format(res.dataset_data));
}

const api = {
  getHistoricalData,
};
export default api;
