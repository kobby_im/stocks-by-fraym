export default {
  companies: [
    { label: "Facebook", value: "FB" },
    { label: "Google", value: "GOOGL" },
    { label: "Tesla", value: "TSLA" },
  ],
  timeFrames: [
    { label: "Day", value: "day" },
    { label: "Week", value: "week" },
    { label: "Month", value: "month" },
  ],
  chartTypes: [
    { label: "CandleSticks", value: "candleSticks" },
    { label: "Line Chart", value: "lineChart" },
  ],
  API_URL: "https://www.quandl.com/api/v3/datasets",
  API_KEY: process.env.API_KEY || "rxBhmzzRJeN7yBoE1es3",
  DEFAULT_CHART_TYPE: "lineChart",
  DEFAULT_COMPANY: "FB",
};
