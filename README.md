# Fraym Historical Stock Price Analyzer

A simple web application that allows users to view historical End of Day stock prices for a selected company. 

## Features

- [ ] Ability to select Company to view its historical Data

- [ ] Ability to select Type of Chart Line Chart or Candle Sticks

- [ ] Pan and Zoom Chart


## Running the application

1. Make sure dependencies are installed. Run:

   ```sh
   yarn
   ```

   Alternatively, you can use `npm install`, although not recommended.

2. Start the application.

   ```sh
   yarn dev
   ```

   This will open the web application at [http://localhost:3000](http://localhost:3000) in your default browser.
   If you don't see the page, open the link to view it in the browser.

## Compatibility

As a web application, this application is expected to work the same in all major browsers on all major platforms.

However, due to resource limitations, this application has only been tested in `Chrome`, `Firefox` and `Opera` on `Macos BigSur 11.1`.

In case you encounter any bugs in other platforms/browsers, file an issue.

## Technical decisions

- `React` selected over `Angular` for fast Prototyping.

- Victory Charts Selected for Charting also for fast prototyping.


## Development

This project was bootstrapped with [Create Nextjs App](https://github.com/facebook/create-react-app).

Built with love ❤️ using [React](https://reactjs.org/), [Nextjs](https://nextjs.org/), [Javascript](https://developer.mozilla.org/en-US/docs/Web/JavaScript) and a couple other helper libraries (see [package.json](./package.json)).

### Project structure

A high level structure of this project files is:

```sh
.
├── README.md
├── components
│   ├── Charts
│   │   └── index.js
│   ├── Filter
│   │   ├── Select.js
│   │   └── index.js
│   └── utils
│       └── index.js
├── lib
│   ├── config.js
│   └── data.js
├── package.json
├── pages
│   ├── _app.js
│   ├── _document.js
│   └── index.js
├── postcss.config.js
├── public
│   ├── favicon.ico
│   └── logo.png
├── styles
│   └── globals.css
├── tailwind.config.js
├── utils
├── yarn-error.log
└── yarn.lock
```

Components (from the `components` directory) are used to compose the `features`.

Features are synonymous to the various pages that a user may interact with.

All configuration-related data are found in the `config.js` module.
Environment variables are extracted ONLY within the config.js file as a best practice to prevent scattered env vars in the codebase.


### Scripts

In the project directory, you can run:

#### `yarn dev`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.


#### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.


## What can be improved?

- Unit tests for components and Functions

- Only Pulling results for visible viewport

- Caching resultings in browser storage and loading from cache when possible

- Predicting the 30 day forecast with a Regresion Algorithm

- Adding a tooltip to make chart easily readable

- Adding a cursor pointer to allow users to see vaue at current pointer position.

- Listen to width changes to resize the chart.

- Overall SVGs tend to take a huge chunk of the memory, the biggest improvement to this will be to use a Canvas based charting solution.


## How I spent my time
- The first `hour and 30 mins` was used to make technical decisions about the project. During this time I read and tried to understand the problem. I also went through the docs for Quandl and researched on other stocks charting platforms like [Tradingview](https://www.tradingview.com/) and [Meta Trader 4](https://www.metatrader4.com/en).

- The next `hour` was used to build the initial layout of the project, with hard coded values.

- The next `hour` was used to build out the library to connect to the API and tested it out with hardcoded data.

- The next `1 hour 30 mins` was used to connect the API to the frontend. During this phase, I fine tuned the UI and the API to adapt to each other.

- I then used `30 mins` to clean up the project making sure it's been formatted correctly.

- I then decided to Explore the selection, by allowing the user to zoom and get data based on the boundary of the chart. I had to make a decision between pulling in the data and panning and Zooming on the client, or getting the data from the server on every Pan and zoom. I presented loading the data for the whole domain and manipulating on the frontend, but this has serious performance constraints especially when the chart is at the candlestick chart type. Ideally I should have implemented a cache/storage layer between the chart and the API and on every change make a call to the buffer or storage, and if it doesn't exit, query the backend for the data. This will be faster, and it will save bandwidth too.


