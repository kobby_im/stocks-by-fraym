import React from 'react';
import '../styles/globals.css';

function MyApp({ Component, pageProps }) {
  return (
    <div>
      <div className="mt-24">
        <Component {...pageProps} />
      </div>
    </div>
  );
}

export default MyApp;
