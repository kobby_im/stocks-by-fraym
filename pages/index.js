import React, { useEffect, useState } from "react";
import Filter from "../components/Filter";
import Charts from "../components/Charts";
import api from "../lib/data";

export default function Home() {
  const [filter, setFillter] = useState({
    company: "FB",
    chartType: "candleSticks",
    startDate: null,
    endDate: null,
  });
  const [data, setData] = useState();
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState();
  useEffect(() => {
    setLoading(true);
    setError(null);
    api
      .getHistoricalData(filter.company, filter.startDate, filter.endDate)
      .then((res) => {
        setData(res);
        setLoading(false);
      })
      .catch((err) => {
        setError(err.message);
      });
  }, [filter]);

  const handleSelectChange = (value) => {
    setFillter(value);
  };

  const handleDateRangeChange = (value) => {
    setFillter((prevState) => {
      const newState = { ...prevState };
      const [start, end] = value;
      newState.startDate = start;
      newState.endDate = end;
      return newState;
    });
  };

  return (
    <div className="font-sans flex flex-col justify-center items-center">
      <div className="flex justify-center items-center">
        <Filter onChange={handleSelectChange} />
      </div>
      {loading && <span className="mt-4">Loading</span>}
      {error && <span className="mt-4">{error}</span>}
      {(!error &&!loading)&& <span className="mt-4">Scroll and Pan to filter Charts</span>}
      <Charts
        onDateRangeChange={handleDateRangeChange}
        type={filter.chartType}
        dataSet={data}
      />
    </div>
  );
}
