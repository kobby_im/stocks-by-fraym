import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import config from '../../lib/config';
import Select from './Select';

const Filter = ({ onChange, initialCompany, initialChartType }) => {
  const [value, setValue] = useState({ company: initialCompany, chartType: initialChartType });

  const handleChange = (nextValue, type) => {
    setValue((prevState) => {
      const newState = { ...prevState };
      newState[type] = nextValue;
      return newState;
    });
  };

  useEffect(() => {
    onChange(value);
  }, [value]);

  return (
    <>
      <Select onChange={(selectedValue) => handleChange(selectedValue, 'company')} selected={value.company} options={config.companies} />
      <Select onChange={(selectedValue) => handleChange(selectedValue, 'chartType')} selected={value.chartType} options={config.chartTypes} />
      {}
    </>
  );
};

Filter.propTypes = {
  initialChartType: PropTypes.string,
  initialCompany: PropTypes.string,
  onChange: PropTypes.func.isRequired,
};

Filter.defaultProps = {
  initialChartType: config.DEFAULT_CHART_TYPE,
  initialCompany: config.DEFAULT_COMPANY,
};

export default Filter;
