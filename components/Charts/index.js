import React, { useEffect, useState } from "react";
import {
  VictoryAxis,
  VictoryChart,
  VictoryLegend,
  VictoryCandlestick,
  VictoryTooltip,
  VictoryTheme,
  VictoryLine,
  VictoryZoomContainer,
} from "victory";
import PropTypes from "prop-types";
import { formatDateTime } from "../../utils";

function Chart({ type, dataSet, onDateRangeChange }) {
  // eslint-disable-line no-unused-vars
  const data = dataSet?.data;
  if (!data || !data.length) {
    return null;
  }

  const [chartHeight, setChartHeight] = useState();
  const [chartWidth, setChartWidth] = useState();

  const getEntireXDomain = () => {
    const min = dataSet.minDate || data[0]?.x;
    const max = dataSet.maxDate || data[data.length - 1]?.x;
    return [min, max];
  };

  const [zoomedXDomain, setZoomedXDomain] = useState(getEntireXDomain());

  const onDomainChange = (domain) => {
    if (domain.x[0] && domain.x[1]) {
      setZoomedXDomain(domain.x);
    }
    // Call update on Change here to load new data from database and pass to component
    // onDateRangeChange(domain.x)
  };

  useEffect(() => {
    setChartHeight(window.innerHeight * 0.8);
    setChartWidth(window.innerWidth);
  }, []);
  return (
    <VictoryChart
      theme={VictoryTheme.material}
      domainPadding={{ x: 50 }}
      scale={{ x: "time" }}
      width={chartWidth}
      height={chartHeight}
      domain={{ x: getEntireXDomain() }}
      containerComponent={
        <VictoryZoomContainer
          onZoomDomainChange={onDomainChange}
          minimumZoom={{ x: 1 / 100 }}
        />
      }
    >
      <VictoryAxis
        tickCount={20}
        // tickFormat={(t) => `${t.getDate()}-${t.getMonth()}-${t.getYear()}`}
        style={{
          axis: {
            stroke: "rgba(0,0,0,0.1)",
            strokeWidth: 1,
          },
          axisLabel: {
            fontSize: 18,
            fontWeight: "bold",
          },
          grid: {
            stroke: "rgba(0,0,0,0.1)",
            strokeDasharray: "",
          },
          ticks: {
            padding: -8,
          },
          tickLabels: {
            fontWeight: "bold",
            fontSize: 10,
          },
        }}
      />

      <VictoryAxis
        dependentAxis
        tickCount={30}
        style={{
          axis: {
            stroke: "rgba(0,0,0,0.1)",
            strokeWidth: 1,
          },
          axisLabel: {
            padding: 40,
            fontSize: 12,
            fontWeight: "bold",
          },
          grid: {
            stroke: "rgba(0,0,0,0.1)",
            strokeDasharray: "",
          },
          tickLabels: {
            fontFamily: '"Montserrat", "sans-serif"',
            fontWeight: "bold",
            fontSize: 9,
          },
        }}
      />

      <VictoryLegend
        title={`${formatDateTime(zoomedXDomain[0]).date} - ${
          formatDateTime(zoomedXDomain[1]).date
        } `}
        x={chartWidth / 2.5}
        y={chartHeight - 20}
        centerTitle
        orientation="horizontal"
        style={{ title: { fontSize: 15, fontWeight: "bold" } }}
      />

      {type === "candleSticks" ? (
        <VictoryCandlestick
          candleColors={{ positive: "#5f5c5b", negative: "#c43a31" }}
          data={data}
        />
      ) : (
        <VictoryLine
          data={data}
          y="close"
          labelComponent={<VictoryTooltip dy={0} />}
          animate={{
            duration: 2000,
            onLoad: { duration: 1000 },
          }}
        />
      )}
    </VictoryChart>
  );
}

Chart.propTypes = {
  type: PropTypes.string,
  dataSet: PropTypes.shape({
    data: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
    minDate: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    maxDate: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  }).isRequired,
  onDateRangeChange: PropTypes.func,
};

Chart.defaultProps = {
  onDateRangeChange: undefined,
  type: undefined,
};

export default Chart;
